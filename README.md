# Netcraft Hackathon #

# Server URL #
```http://netcraft-hackathon.herokuapp.com/```

## API ##

# GET requests: #

```
http://netcraft-hackathon.herokuapp.com/posts
```
* will return all posts.

# POST requests #

```
http://netcraft-hackathon.herokuapp.com/register
```
* will register a new user for the session
```
{
    name : String
}
```




```
http://netcraft-hackathon.herokuapp.com/post
```
* will create a new post. requires register
## Requires Body ## 

```
{
    message : String
}
```


```
http://netcraft-hackathon.herokuapp.com/comment
```
* will create a new comment. requires register
## Requires Body ## 

```
{
    message : String,
    postId : String
}
```


```
http://netcraft-hackathon.herokuapp.com/like
```
* will like a new comment or post, or unlike them. requires register
## Requires Body ## 

```
{
    id : String,
}
```

