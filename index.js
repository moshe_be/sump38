// ./src/index.js

// importing the dependencies
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const helmet = require('helmet');
const morgan = require('morgan');
const session = require('express-session');

// defining the Express app
const app = express();

app.use(cors());
/*
post:
{
  name : string,
  message : string,
  likes : [],
  comments : [],
  time : date,
  id : string
}
*/




const posts = [];

const users = [];

const getUser = (sessionId) => {
  return users.find(user => user.id === sessionId);
}





// adding Helmet to enhance your API's security
app.use(helmet());

// using bodyParser to parse JSON bodies into JS objects
app.use(bodyParser.json());

// enabling CORS for all requests

// adding morgan to log HTTP requests
app.use(morgan('combined'));


app.use(session({
  secret: 'netcraft hackathon',
  resave: false,
  saveUninitialized: true,
  cookie: { secure: false }
}))


// defining an endpoint to return all ads
app.get('/', (req, res) => {
  res.send(getUser(req.session.id));
});

app.get('/posts', (req, res) => {
  res.send(posts);
});


/*
post post:
{
  message : string
}
*/

const handleUserExists = (req, res) =>{
  const user = getUser(req.session.id);
  if(user) {
    return user;
  }
  else {
    res.send(401, 'no user registered');
    return null;
  }
};




app.post('/post', (req,res) => {
    const user = handleUserExists(req,res);
    if(user){
      if(req.body.message) {
        const newPost = {
          name : user.name,
          message : req.body.message,
          likes : [],
          comments : [],
          time : new Date(),
          id : '' + posts.length
        };
        posts.push(newPost);
        res.send(newPost);
      } else {
        res.send('failed');
      }
    }
});

/*
  comment post:
  {
    message : string,
    postId : string
  }

*/

app.get('/', (req,res) => {
    res.send('server works');
})


app.post('/like', (req, res) => {
  const user = handleUserExists(req,res);
  if(user){
    if(req.body.id !== undefined) {
      const _id = req.body.id;
      if(_id.includes('_')) { // comment id
        const postId = _id.split('_')[0];
        let commentLikes = posts.find(post => post.id === postId);
        commentLikes = commentLikes.comments.find(comment => comment.id === _id);
        commentLikes = commentLikes.likes;
        const currentLikeIndex = commentLikes.findIndex(like => like.name === user.name);
        if(currentLikeIndex != -1) {
          commentLikes.splice(currentLikeIndex, 1);
        }
        else {
          commentLikes.push({
            name : user.name
          })
        }
      
      }
      else {
        const postLikes = posts.find(post => post.id === _id).likes;
        const currentLikeIndex = postLikes.findIndex(like => like.name === user.name);
        if(currentLikeIndex != -1) {
          postLikes.splice(currentLikeIndex, 1);
        }
        else {
          postLikes.push({
            name : user.name
          })
        }
      }
      res.send('success');
    } else {
      res.send('no such element');
    }
  }
})


app.post('/comment', (req, res) => {
  const user = handleUserExists(req,res);
  if(user){
    if(req.body.message && req.body.postId !== undefined) {
      const post = posts.find(post => post.id === req.body.postId);
      if(!post) {
        res.send('post does not exist');
      } else {
        newComment = {
          name : user.name,
          message : req.body.message,
          time : new Date(),
          id : post.id + '_' + post.comments.length,
          likes : []
        };
        post.comments.push(newComment);
        res.send(newComment);
      }
    } else {
      res.send('failed');
    }
  }
});


app.post('/register', (req, res) => {
  if(req.body.name) {
    const user = getUser(req.session.id);
    if(!user) {
      users.push({
        id : req.session.id,
        name : req.body.name
      });
      res.send('success');
    } else {
      user.name = req.body.name;
      res.send('name changed');
    }
  }
  else res.send('failed');
})





// starting the server
app.listen(process.env.PORT || 8080, ()=>{
    console.log(process.env.PORT);
});